import React, {createContext, useContext, useState} from "react";

const UserContext = createContext('')

export const useUser = () => {
    return useContext(UserContext)
}

const UserProvider = ({ children }) => {

    const [ username, setUsername ] = useState('')
    const [ userId, setUserId ] = useState('')

    return (
        <UserContext.Provider value={ { usernameState: {username, setUsername }, userIdState: {userId, setUserId}} }>
            { children }
        </UserContext.Provider>
    )
}
export default UserProvider
