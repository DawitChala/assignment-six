import React, {createContext, useContext, useState} from "react";

const TranslationContext = createContext('')

export const useTranslation = () => {
    return useContext(TranslationContext)
}

const TranslationProvider = ({ children }) => {

    const [ translations, setTranslations ] = useState([])

    return (
        <TranslationContext.Provider value={ { translations,  setTranslations } }>
            { children }
        </TranslationContext.Provider>
    )
}
export default TranslationProvider
