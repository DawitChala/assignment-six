import UserProvider from "./UserContext";
import TranslationProvider from "./TranslationContext";
const AppContext = ({children}) => {
    return(
        <TranslationProvider>
            <UserProvider>
                {children}
            </UserProvider>
        </TranslationProvider>

    )
}
export default AppContext;