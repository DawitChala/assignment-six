import Header from "./Header";
import {useEffect, useState} from "react";
import {useTranslation} from "../context/TranslationContext";
import {UserAPI} from "../Api/UserAPI";
import {useUser} from "../context/UserContext";
import {useHistory} from "react-router-dom";

function Translations() {
    const [localTranslation, setLocalTranslation] = useState("");
    const [charArray, setCharArray] = useState([])
    const { usernameState, userIdState } = useUser()
    const { username } = usernameState
    const { userId } = userIdState
    const {translations, setTranslations} = useTranslation()
    const history = useHistory()

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        let tempCharArray = localTranslation.toLowerCase().split('')
        tempCharArray = tempCharArray.filter(char => {return(char.match(/[a-z]/i))})
        tempCharArray = tempCharArray.map(char => `/individual_signs/${char}.png`)
        setCharArray(tempCharArray)

        let tempTranslations = [...translations]
        tempTranslations.push(localTranslation)
        setTranslations(tempTranslations)
        await UserAPI.updateTranslations(userId, tempTranslations)
    }

    useEffect(() => {

        if (username === '') {
            history.push('/')
        }
    }, [ username, history ])

    return (
        <div className="container">
            <Header/>
            <h1>Translation</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    Translate:
                    <textarea className="materialize-textarea"
                        value={localTranslation}
                        onChange={e =>{
                            setLocalTranslation(e.target.value)}}
                    />
                </label>
                <button className="waves-effect waves-light btn" style={{display: 'flex'}} type="submit" value="Submit">Translate</button>
            </form>
            <ul>
                { charArray.map((char, index) => <span key={index}>

                    <img src={char} alt="Pikk" height={100}/>
                </span>) }
            </ul>


        </div>
    );
}

export default Translations;