import {useUser} from "../context/UserContext";
import {useTranslation} from "../context/TranslationContext";
import {Link} from "react-router-dom";

const Header=()=> {
    const { usernameState, userIdState } = useUser()
    const { username, setUsername} = usernameState
    const {  setUserId} = userIdState
    const {  setTranslations } = useTranslation()


    const logOut = () => {
        setUsername('')
        setUserId('')
        setTranslations([])

    }

    return (
        <nav>
            <div className="nav-wrapper">
                <Link className="brand-logo" style={{marginLeft: "10px"}} to="/">Lost in translation</Link>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><Link to="/profile">{ username }</Link></li>
                    {username?
                        <li><Link onClick={logOut} to="/">Log out</Link></li>
                        :
                        ""
                    }
                </ul>
            </div>
        </nav>


    )
}

export default Header;