import {useUser} from "../context/UserContext";
import {useTranslation} from "../context/TranslationContext";
import Header from "./Header";
import {UserAPI} from "../Api/UserAPI";

const Profile=()=> {
    const { usernameState, userIdState } = useUser()
    const { username} = usernameState
    const { userId} = userIdState
    const { translations,setTranslations} = useTranslation();


    const handleSubmit = async (evt) => {
        evt.preventDefault();
        setTranslations([])
        await UserAPI.updateTranslations(userId, [])
    }


    return (
        <div className="container">
            <Header/>
            <h2>{ username }</h2>
            <ul className="collection">
                { translations.slice(-10).map(translation => <li className="collection-item" key={ translation }>{ translation }</li>) }
            </ul>
            <form onSubmit={handleSubmit}>
                <button className="waves-effect waves-light btn" type="submit" value="Submit">DELETE TRANSLATIONS</button>
            </form>
        </div>
    );
}

export default Profile;