import {UserAPI} from "../Api/UserAPI";
import Header from "./Header";
import  {useUser} from "../context/UserContext";
import React, {useEffect, useState} from "react";
import {useTranslation} from "../context/TranslationContext";
import {useHistory} from "react-router-dom";


function Login() {
    const { usernameState, userIdState } = useUser()
    const { username, setUsername} = usernameState
    const {  setUserId} = userIdState
    const { setTranslations} = useTranslation();
    const [localUsername, setLocalUsername] = useState('');
    const history = useHistory()

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        setUsername(localUsername)
        const user = await UserAPI.login(localUsername)
        setUserId(user.id)
        setTranslations(user.translations);
    }

    useEffect(() => {

        if (username !== '') {
            history.push('/translation')
        }
    }, [ username,history ])


    return (

        <div className="container">
            <Header/>
            <form onSubmit={handleSubmit}>
                <label>
                    Username:
                    <input
                        type="text"
                        value={localUsername}
                        onChange={e =>{
                            setLocalUsername(e.target.value)}}
                    />
                </label>
                <input className="waves-effect waves-light btn" type="submit" value="Submit" />
            </form>

        </div>

    );
}

export default Login;
