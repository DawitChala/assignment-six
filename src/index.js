import ReactDOM from 'react-dom';
import App from "./App";
import React from "react";
import AppContext from "./context/AppContext";
import 'materialize-css/dist/css/materialize.min.css'

ReactDOM.render(
    <React.StrictMode>
        <AppContext>
            <App/>
        </AppContext>
    </React.StrictMode>,
    document.getElementById('root')
)
