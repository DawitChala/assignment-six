import {BrowserRouter, Route} from "react-router-dom";
import Login from "./components/Login";
import Profile from "./components/Profile";
import Translation from "./components/Translation";

function App() {
  return (
        <div>
            <BrowserRouter >
                <Route exact path="/" component={Login} />
                <Route path="/profile" component={Profile}/>
                <Route path="/translation" component={Translation}/>
            </BrowserRouter>
        </div>
  )
}

export default App;
