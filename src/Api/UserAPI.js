const apiKey = "6d6c7ffe-a09e-4d93-8585-6c5c72493b93"
const apiUrl = "https://hkon-assignment-api.herokuapp.com/translations"
export const UserAPI = {
    async login(username){
        const user = await fetch(`${apiUrl}?username=${username}`)
            .then(response => response.json());
        if(user.length !== 0) return user[0]

        const requestOptions =  {
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                translations: []
            })
        }


        return await fetch(apiUrl,requestOptions)
            .then(response => response.json())

    },

    async updateTranslations(userid, trans){

        const requestOptions =  {
            method: 'PATCH',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                translations: trans
            })
        }


        return await fetch(`${apiUrl}/${userid}`,requestOptions)
            .then(response => response.json())
    }



}